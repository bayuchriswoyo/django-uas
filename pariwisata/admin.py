from django.contrib import admin
from .models import Wisata

# Register your models here.
@admin.register(Wisata)
class WisataAdmin(admin.ModelAdmin):
    list_display = ['nama_wisata', 'kategori', 'tiket', 'fasilitas',
                    'tgl_input', 'keterangan', 'user']
    list_filter = ['kategori', 'user']
    search_fields = [['nama_wisata', 'kategori', 'user']]