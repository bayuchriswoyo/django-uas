from .models import Wisata
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView, View
from .utils import Render

# Create your views here.
var = {
'judul' : 'Pariwisata Kediri',
'subjudul' : '''Ini adalah data pariwisata yang ada di area Kediri''',

}

def index(self):
	var['wisata'] = Wisata.objects.values('id','nama_wisata','kategori','alamat','fasilitas','keterangan','tiket').\
	order_by('nama_wisata')
	return render(self, 'pariwisata/index.html',context=var)

def about(self):
	return render(self, 'pariwisata/about.html',context=var)

class WisataDetailView(DetailView):
	model = Wisata
	template_name = 'pariwisata/tmpwisata_detail_view.html'

	def get_context_data(self, **kwargs):
		context=var
		context.update(super().get_context_data(**kwargs))
		return context
		
class WisataCreateView(CreateView):
		model = Wisata
		fields = '__all__'
		template_name = 'pariwisata/tmpwisata_add.html'

def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class WisataEditView(UpdateView):
	model = Wisata
	fields = ['nama_wisata','kategori','keterangan','alamat','fasilitas','keterangan','tiket']
	template_name = 'pariwisata/tmpwisata_edit.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class WisataDeleteView(DeleteView):
	model = Wisata
	template_name = 'pariwisata/tmpwisata_delete.html'
	success_url = reverse_lazy('home_page')

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class WisataToPdf(View):
	def get(self, request):
		var = {
			'wisata' : Wisata.objects.values('nama_wisata','kategori','alamat','fasilitas','keterangan','tiket'),
			'request' : request
		}
		return Render.to_pdf(self, 'pariwisata/tmpwisata_to_pdf.html', var)