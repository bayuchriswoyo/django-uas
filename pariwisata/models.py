from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone

# Create your models here.

class Wisata(models.Model):
    KATEGORI_CHOICES = (
        ('air','Wisata Air'),
        ('alm','Wisata Alam'),
        ('tmn','Taman'),
        ('mon','Monumen'),
    )
    nama_wisata = models.CharField('Nama Wisata', max_length=50, null=False)
    kategori = models.CharField(max_length=3, choices=KATEGORI_CHOICES)
    keterangan = models.TextField(default='')
    fasilitas = models.TextField(default='')
    alamat = models.TextField(default='')
    tiket = models.FloatField('Harga Tiket (Rp)')
    tgl_input = models.DateTimeField('Tgl. Edit', default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl_input']
    
    def __str__(self):
        return self.nama_wisata

    def get_absolute_url(self):
        return reverse('home_page')