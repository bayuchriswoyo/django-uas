from django.urls import path
from .views import index, about, WisataDetailView, WisataCreateView
from .views import WisataEditView, WisataDeleteView, WisataToPdf

urlpatterns = [
    path('', index, name='home_page'),
    path('pariwisata/about', about, name='about'),
    path('pariwisata/<int:pk>', WisataDetailView.as_view(), name='tmpwisata_detail_view'),
    path('pariwisata/add', WisataCreateView.as_view(), name='tmpwisata_add'),
    path('pariwisata/edit/<int:pk>', WisataEditView.as_view(), name='tmpwisata_edit'),
    path('pariwisata/delete<int:pk>', WisataDeleteView.as_view(), name='tmpwisata_delete'),
    path('pariwisata/print_pdf', WisataToPdf.as_view(), name='tmpwisata_to_pdf'),
]